import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TodoService } from '../services/todo.service';
import { Todo } from '../ITodo';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})

export class UpdateComponent implements OnInit {
  id: number;
  todo: Todo;
  todoForm = new FormGroup({
    id: new FormControl(),
    title: new FormControl(),
    completed: new FormControl()
  });
  messages: string[];

  constructor(
    private todoService: TodoService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  updateTodo(todoForm: FormGroup): void {
    this.messages = [];
    const title = 'title';
    if (todoForm.value[title] === '') {
      this.messages.push('Please enter todo title');
    } else {
      const form = JSON.stringify(todoForm.value);
      const observable = this.todoService.updateTodo(form);
      observable.subscribe(data => {
        this.router.navigate(['/todos']);
      }, err => {
        this.messages.push(err.error.error);
      });
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = 'id';
      this.id = params[id];
    });

    const observable = this.todoService.getTodo(this.id);
    observable.subscribe(data => {
      this.todo = data;
      this.todoForm.get('id').setValue(data.id);
      this.todoForm.get('title').setValue(data.title);
      this.todoForm.get('completed').setValue(data.completed);
    });
  }
}
