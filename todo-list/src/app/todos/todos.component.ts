import { Component, OnInit, DoCheck } from '@angular/core';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todoList = [];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    const observable = this.todoService.getTodos();
    observable.subscribe(data => {
      this.todoList = data;
    });
  }
}
