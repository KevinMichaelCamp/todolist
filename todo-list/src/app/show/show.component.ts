import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TodoService } from '../services/todo.service';
import { Todo } from '../ITodo';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})

export class ShowComponent implements OnInit {
  id: number;
  todo: Todo;

  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  markComplete(): void {
    this.todoService.completeTodo(this.id).subscribe(data => {
      this.router.navigate(['/todos']);
    });
  }

  deleteTodo(): void {
    const observable = this.todoService.deleteTodo(this.id);
    observable.subscribe(data => {
      this.router.navigate(['/todos']);
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = 'id';
      this.id = params[id];
    });

    const observable = this.todoService.getTodo(this.id);
    observable.subscribe(data => {
      this.todo = data;
    });
  }

}
