import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})

export class CreateComponent implements OnInit {
  todoForm = new FormGroup({
    title: new FormControl()
  });
  messages: string[];

  constructor(private todoService: TodoService, private router: Router) { }

  createTodo(todoForm: FormGroup): void {
    this.messages = [];
    const title = 'title';
    if (todoForm.value[title]) {
      this.messages.push('Please enter todo title');
    } else {
      const form = JSON.stringify(todoForm.value);
      const observable = this.todoService.createTodo(form);
      observable.subscribe(data => {
        this.router.navigate(['/todos']);
      }, err => {
        this.messages.push(err.error.error);
      }
      );
    }
  }

  ngOnInit(): void {

  }

}
