import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'yesorno'
})

export class YesOrNoPipe implements PipeTransform {
    transform(bool: boolean): string {
        if (bool) {
            return 'YES';
        }
        return 'NO';
    }
}
