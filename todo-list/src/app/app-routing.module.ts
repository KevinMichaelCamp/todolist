import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { TodosComponent } from './todos/todos.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { ShowComponent } from './show/show.component';


const routes: Routes = [
  { path: 'todos', component: TodosComponent },
  { path: 'todos/new', component: CreateComponent },
  { path: 'todos/edit/:id', component: UpdateComponent },
  { path: 'todos/:id', component: ShowComponent },
  { path: '', pathMatch: 'full', component: WelcomeComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
