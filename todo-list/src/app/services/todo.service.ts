import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../ITodo';

@Injectable({
  providedIn: 'root'
})

export class TodoService {

  constructor(private http: HttpClient) { }

  getTodos(): Observable<string[]> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get<string[]>('http://ec2-18-216-168-37.us-east-2.compute.amazonaws.com:8080/todos', httpHead);
  }

  getTodo(id: number): Observable<Todo> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get<Todo>(`http://ec2-18-216-168-37.us-east-2.compute.amazonaws.com:8080/todos/${id}`, httpHead);
  }

  createTodo(todoForm): Observable<string[]> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post<string[]>('http://ec2-18-216-168-37.us-east-2.compute.amazonaws.com:8080/todos', todoForm, httpHead);
  }

  updateTodo(todoForm): Observable<string> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.put<string>('http://ec2-18-216-168-37.us-east-2.compute.amazonaws.com:8080/todos', todoForm, httpHead);
  }

  completeTodo(id: number): Observable<string> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.patch<string>(`http://ec2-18-216-168-37.us-east-2.compute.amazonaws.com:8080/todos/${id}`, httpHead);
  }

  deleteTodo(id: number): Observable<string> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.delete<string>(`http://ec2-18-216-168-37.us-east-2.compute.amazonaws.com:8080/todos/${id}`, httpHead);
  }
}
