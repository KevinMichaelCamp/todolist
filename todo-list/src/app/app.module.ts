import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { YesOrNoPipe } from './pipes/yesorno.pipe';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NavigationComponent } from './navigation/navigation.component';
import { TodosComponent } from './todos/todos.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { ShowComponent } from './show/show.component';
import { TodoService } from './services/todo.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavigationComponent,
    TodosComponent,
    CreateComponent,
    UpdateComponent,
    ShowComponent,
    YesOrNoPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
